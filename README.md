[![Set Solutions](images/setsolutions_banner.png)](https://www.setsolutions.com)
# CI/CD Demo Application for Splunk

___Version 22.2.2784.54633___

The Set Solutions Splunk demo app may be used for demonstration and/or training purposes. It may also be used as a generic codebase for building other Splunk applications. The application provides a basic demonstration of some of Bitbucket's CI/CD pipeline capabilities. The normal CI/CD pipeline process for this demonstration is that:

- All development work should be performed as feature/changes;
- These features are implemented in development via pull requests;
- Once a certain minimum number or a set date has occurred, the development changes are merged to release.

Upon a successful release build, the resulting Splunk app package will be automatically submitted to Splunkbase.

## Installation

TODO: Describe the installation process

## Usage

TODO: Write usage instructions

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

- Change history can be published here.
- Feature/Bugfix with link to ticket management system.
- Ideally updating this will be an automated process.

### Contacts

* [Repository Admin](mailto:admin@setsolutions.com?subject=Public%20Repository:%20CI/CD%20Demo%20App%20for%20Splunk)
* [Development Team](mailto:dev@setsolutions.com?subject=CI/CD%20Demo%20App%20for%20Splunk%20Question)

## License

MIT License

Copyright (c) 2022 Set Solutions, Inc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
